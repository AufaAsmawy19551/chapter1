import { rl, input } from '../utils/input.mjs';

export async function fiturVolumeTabung() {
  console.log('\nVOLUME TABUNG');
  console.log('=============');

  try {
    const jariJariAlas = parseFloat(await input('Masukkan jari-jari alas: '));
    const tinggiTabung = parseFloat(await input('Masukkan tinggi tabung: '));

    console.log(`Volume tabung adalah ${Math.PI} * ${jariJariAlas} ** 2 * ${tinggiTabung} = ${Math.PI * jariJariAlas ** 2 * tinggiTabung}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturVolumeTabung();
//   rl.close();
// }

// test();
