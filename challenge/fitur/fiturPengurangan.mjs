import { rl, input } from '../utils/input.mjs';

export async function fiturPengurangan() {
  console.log('\nPENGURANGAN');
  console.log('===========');

  try {
    const angkaPertama = parseFloat(await input('Masukkan angka pertama: '));
    const angkaKedua = parseFloat(await input('Masukkan angka kedua: '));

    console.log(`Hasil pengurangan ${angkaPertama} - ${angkaKedua} = ${angkaPertama - angkaKedua}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturPengurangan();
//   rl.close();
// }

// test();
