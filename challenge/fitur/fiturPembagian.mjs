import { rl, input } from '../utils/input.mjs';

export async function fiturPembagian() {
  console.log('\nPEMBAGIAN');
  console.log('=========');

  try {
    const angkaPertama = parseFloat(await input('Masukkan angka pertama: '));
    const angkaKedua = parseFloat(await input('Masukkan angka kedua: '));

    console.log(`Hasil pembagian ${angkaPertama} : ${angkaKedua} = ${angkaPertama / angkaKedua}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturPembagian();
//   rl.close();
// }

// test();
