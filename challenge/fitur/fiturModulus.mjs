import { rl, input } from '../utils/input.mjs';

export async function fiturModulus() {
  console.log('\nMODULUS');
  console.log('=======');

  try {
    const angkaPertama = parseFloat(await input('Masukkan angka pertama: '));
    const angkaKedua = parseFloat(await input('Masukkan angka kedua: '));

    console.log(`Hasil modulus ${angkaPertama} % ${angkaKedua} = ${angkaPertama % angkaKedua}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturModulus();
//   rl.close();
// }

// test();
