import { rl, input } from '../utils/input.mjs';

export async function fiturAkarKuadrat() {
  console.log('\nAKAR KUADRAT');
  console.log('============');

  try {
    const angkaPertama = parseFloat(await input('Masukkan angka pertama: '));

    console.log(`Hasil akar kuadrat √${angkaPertama} = ${Math.sqrt(angkaPertama)}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturAkarKuadrat();
//   rl.close();
// }

// test();
