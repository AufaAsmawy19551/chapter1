import { rl, input } from '../utils/input.mjs';

export async function fiturVolumeKubus() {
  console.log('\nVOLUME KUBUS');
  console.log('============');

  try {
    const panjangSisi = parseFloat(await input('Masukkan panjang sisi: '));

    console.log(`Volume kubus adalah ${panjangSisi} ** 3 = ${panjangSisi ** 3}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturVolumeKubus();
//   rl.close();
// }

// test();
