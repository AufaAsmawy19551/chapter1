import { rl, input } from '../utils/input.mjs';

export async function fiturPerkalian() {
  console.log('\nPERKALIAN');
  console.log('=========');

  try {
    const angkaPertama = parseFloat(await input('Masukkan angka pertama: '));
    const angkaKedua = parseFloat(await input('Masukkan angka kedua: '));

    console.log(`Hasil perkalian ${angkaPertama} x ${angkaKedua} = ${angkaPertama * angkaKedua}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturPerkalian();
//   rl.close();
// }

// test();
