import { rl, input } from '../utils/input.mjs';

export async function fiturLuasPersegi() {
  console.log('\nLUAS PERSEGI');
  console.log('============');

  try {
    const panjangSisi = parseFloat(await input('Masukkan panjang sisi: '));

    console.log(`Luas persegi adalah ${panjangSisi} ** 2 = ${panjangSisi ** 2}`);
    console.log('===============================================');
  } catch (err) {
    console.log(err.message);
  }
}

// async function test() {
//   await fiturLuasPersegi();
//   rl.close();
// }

// test();
