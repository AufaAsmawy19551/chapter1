import { rl, input } from './utils/input.mjs';
import { fiturPenjumlahan } from './fitur/fiturPenjumlahan.mjs';
import { fiturPengurangan } from './fitur/fiturPengurangan.mjs';
import { fiturPerkalian } from './fitur/fiturPerkalian.mjs';
import { fiturPembagian } from './fitur/fiturPembagian.mjs';
import { fiturModulus } from './fitur/fiturModulus.mjs';
import { fiturAkarKuadrat } from './fitur/fiturAkarKuadrat.mjs';
import { fiturLuasPersegi } from './fitur/fiturLuasPersegi.mjs';
import { fiturVolumeKubus } from './fitur/fiturVolumeKubus.mjs';
import { fiturVolumeTabung } from './fitur/fiturVolumeTabung.mjs';

async function main() {
  try {
    console.log('\nSELAMAT DATANG DI APLIKASI KALKULATOR SEDERHANA');
    console.log('===============================================');
    while (true) {
      let angka = await input(
        '\nMENU UTAMA\n' +
          '==========\n' +
          '1. Penjumlahan\n' +
          '2. Pengurangan\n' +
          '3. Perkalian\n' +
          '4. Pembagian\n' +
          '5. Modulus\n' +
          '6. Akar Kuadrat\n' +
          '7. Luas Persegi\n' +
          '8. Volume Kubus\n' +
          '9. Volume Tabung\n' +
          '0. Keluar\n' +
          'Masukkan pilihan: '
      );

      console.log('===============================================');

      if (angka == 1) {
        await fiturPenjumlahan();
      } else if (angka == 2) {
        await fiturPengurangan();
      } else if (angka == 3) {
        await fiturPerkalian();
      } else if (angka == 4) {
        await fiturPembagian();
      } else if (angka == 5) {
        await fiturModulus();
      } else if (angka == 6) {
        await fiturAkarKuadrat();
      } else if (angka == 7) {
        await fiturLuasPersegi();
      } else if (angka == 8) {
        await fiturVolumeKubus();
      } else if (angka == 9) {
        await fiturVolumeTabung();
      } else if (angka == 0) {
        console.log('\nKeluar');
        break;
      } else {
        console.log('\nTidak Dimengerti');
      }
    }

    rl.close();
  } catch (err) {
    console.log(err.message);
    rl.close();
  }
}

main();
